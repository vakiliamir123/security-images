# Run this manualy

For now, it's a bit tricky :)

1. Set up DefectDojo variables
```
export SEC_DD_URL=<defectdojo_url>
export SEC_DD_KEY=<defectdojo_key>
```

2. Open any finding in defectdojo related to the project dependencies vulnerabilities
3. Open it's engagement to copy it's ID
`export DD_ENGAGEMENT_ID=<that_it>`

4. Do `git clone` of your vulnerable project
5. Execute patcher

**For python**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
python3 -m pip_audit -r requirements.txt --fix
```

**For JS**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
npm audit fix --force
```

**For GoLang**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
go get -u && go mod tidy
```

6. Execute simplification script

**For python**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
-e SEC_DD_URL=$SEC_DD_URL \
-e SEC_DD_KEY=$SEC_DD_KEY \
-e DD_ATTACHMENT_FILE_NAME=requirements.txt \
-e DD_ENGAGEMENT_ID=$DD_ENGAGEMENT_ID \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
python3 /simplifier/dependency-task-simplifier.py
```

**For JS**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
-e SEC_DD_URL=$SEC_DD_URL \
-e SEC_DD_KEY=$SEC_DD_KEY \
-e DD_ATTACHMENT_FILE_NAME=package.json \
-e DD_ENGAGEMENT_ID=$DD_ENGAGEMENT_ID \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
python3 /simplifier/dependency-task-simplifier.py
```

**For GoLang**

```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
-e SEC_DD_URL=$SEC_DD_URL \
-e SEC_DD_KEY=$SEC_DD_KEY \
-e DD_ATTACHMENT_FILE_NAME=go.mod \
-e DD_ENGAGEMENT_ID=$DD_ENGAGEMENT_ID \
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
python3 /simplifier/dependency-task-simplifier.py
```

## Notes

You can avoid step 5 and execute simplifier with ATTACH_FILE=False option:

```
```
docker run -it --rm --name=ws-patcher -v $(pwd):$(pwd) \
-e SEC_DD_URL=$SEC_DD_URL \
-e SEC_DD_KEY=$SEC_DD_KEY \
-e DD_ATTACHMENT_FILE_NAME=requirements.txt \
-e DD_ENGAGEMENT_ID=$DD_ENGAGEMENT_ID \
-e ATTACH_FILE=False \ 
registry.gitlab.com/whitespots-public/security-images/ws-patcher:latest \
python3 /simplifier/dependency-task-simplifier.py
```
```
